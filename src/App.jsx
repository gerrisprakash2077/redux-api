import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import Home from './Components/Home'
import CreateUser from './Components/CreateUser'
import ListUser from './Components/ListUser'
export default class App extends Component {
  render() {
    return (
      <div>
        <Route path='/' exact>
          <Home />
        </Route>
        <Route path='/ListUser' >
          <ListUser />
        </Route>
        <Route path='/CreateUser'>
          <CreateUser />
        </Route>
      </div>
    )
  }
}
