import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { setEmail, setPassword, post } from '../Redux'
class CreateUser extends Component {
    submitHandler = (event) => {
        event.preventDefault()
        console.log(this.props.state.postId);
        this.props.post(this.props.state.email, this.props.state.password)
        // this.props.setEmailAndPassword()
    }
    email = (event) => {
        this.props.setEmail(event.target.value)
    }
    password = (event) => {
        this.props.setPassword(event.target.value)
    }
    reset = () => {

    }
    render() {
        return (
            <div>
                <NavLink to='/'>
                    <button onClick={this.reset} className='ml-10 mt-10 bg-red-600 px-5 py-2 rounded-xl'>Go to Home</button>
                </NavLink>
                <h2 className='text-center my-10 text-5xl text-white font-black'>Create <span className='text-red-600'>User</span></h2>
                <form className='flex flex-col w-max ml-auto mr-auto border-4
                 border-gray-600 rounded-3xl p-10' >
                    <div className='mb-4'>
                        <label className='inline-block w-20 text-xl text-white' htmlFor="email">Email:</label>
                        <input onChange={this.email} required className='border-2 border-gray-400 bg-black px-4 py-1 rounded-xl' type='email' id="email" placeholder='Enter your email' />
                    </div>
                    <div>
                        <label className='inline-block w-20 text-white' htmlFor="password">Password:</label>
                        <input onChange={this.password} required className='border-2 border-gray-400 bg-black px-4 py-1 rounded-xl' type="password" placeholder='Enter your password' id="password" />
                    </div>
                    <button className=' w-30 ml-auto mr-auto mt-5 bg-red-600 text-white px-5 py-2 rounded-xl' onClick={this.submitHandler} type="submit">Create</button>
                </form>
                <h2 className='text-center mt-10'>{this.props.state.postId}</h2>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        state: state
    }
}
const mapDispatchToProps = {
    setEmail: setEmail,
    setPassword: setPassword,
    post: post
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser)
