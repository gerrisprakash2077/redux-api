import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
export default class Home extends Component {
    render() {
        return (
            <div>
                <h1 className='text-center p-4 bg-red-600 text-4xl'>Handling API With Redux</h1>
                <div className='mt-10 text-center'>
                    <NavLink to='/ListUser'>
                        <button className='mr-10 bg-red-600 px-5 py-2 rounded-xl'>List Users</button>
                    </NavLink>
                    <NavLink to='/CreateUser'>
                        <button className='mr-10 bg-red-600 px-5 py-2 rounded-xl'>Create Users</button>
                    </NavLink>
                </div>
            </div>
        )
    }
}
