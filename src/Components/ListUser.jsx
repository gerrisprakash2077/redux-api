import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchPage, fetchUser, resetData } from '../Redux'
// let pageNo = ''
let flag = false

class ListUser extends Component {
    constructor(props) {
        super(props)
        this.peopleflag = false
        this.personFlag = false
        this.pageNo = ''
        this.randomUser = ''
    }
    // componentDidMount() {
    //     this.props.fetchPage(`https://reqres.in/api/users?page=${pageNo}`)
    // }
    displayUser = () => {
        // console.log(this.props.state.data);
        // return
        if (this.props.state.data != 'LOADING...' && this.props.state.data != '' && flag == false) {
            return this.props.state.data.map((person) => {
                return <div key={person.id} >
                    <h3 className='text-center'>{person.first_name}</h3>
                    <p className='text-center my-4'>{person.email}</p>
                    <img className='ml-auto mr-auto w-30 h-30' src={person.avatar} alt="" />
                </div>
            })
        }
        else if (this.props.state.data != 'LOADING...' && this.props.state.data != '' && flag == true) {
            // console.log(this.props.state.data);
            return <div key={this.props.state.data.id} >
                <h3 className='text-center'>{this.props.state.data.first_name}</h3>
                <p className='text-center my-4'>{this.props.state.data.email}</p>
                <img className='ml-auto mr-auto w-30 h-30' src={this.props.state.data.avatar} alt="" />
            </div>
        }
    }
    userByPageHandler = () => {
        this.randomUser = ''
        flag = false
        if (this.pageNo == 1) {

            this.pageNo = 2
        } else if (this.pageNo == 2) {
            this.pageNo = 1
        } else {
            this.pageNo = 1
        }
        this.peopleflag = true
        this.personFlag = false
        this.props.fetchPage(`https://reqres.in/api/users?page=${this.pageNo}`)
    }
    singleUserHandler = () => {
        this.pageNo = ''
        flag = true
        this.peopleflag = false
        this.personFlag = true
        this.randomUser = Math.floor(Math.random() * 9) + 1
        console.log(this.randomUser);
        this.props.fetchPage(`https://reqres.in/api/users/${this.randomUser}`)
    }
    render() {
        return (
            <div>
                <NavLink to='/'>
                    <button onClick={this.props.resetData} className='ml-10 mt-10 bg-red-600 px-5 py-2 rounded-xl'>Go to Home</button>
                </NavLink>
                <div className='text-center mt-10'>
                    <button onClick={this.userByPageHandler} className={this.peopleflag == true ? 'mr-10 bg-white text-red-600 px-5 py-2 rounded-xl' : 'mr-10 bg-red-600 px-5 py-2 rounded-xl'} >USERS BY PAGE</button>
                    <button onClick={this.singleUserHandler} className={this.personFlag == true ? 'mr-10 bg-white text-red-600 px-5 py-2 rounded-xl' : 'mr-10 bg-red-600 px-5 py-2 rounded-xl'}>USERS BY ID</button>
                </div>
                <div>
                    <h2 className={this.props.state.data == 'LOADING...' ? 'text-center m-10 text-3xl' : 'hidden'}>Loading...</h2>
                    <h2 className={this.pageNo ? "text-center mt-20 text-2xl" : "hidden"}>PAGE <span className='text-red-600 font-bold'>NO</span> : {this.pageNo}</h2>
                    <h2 className={this.randomUser ? "text-center mt-20 text-2xl" : "hidden"}>USER <span className='text-red-600 font-bold'>ID</span> : {this.randomUser}</h2>
                    <div className='flex justify-center gap-10 mt-24 flex-wrap'>
                        {this.displayUser()}

                    </div>
                </div>

            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        state: state
    }
}
const mapDispatchToProps = {
    fetchPage: fetchPage,
    fetchUser: fetchUser,
    resetData: resetData
}

export default connect(mapStateToProps, mapDispatchToProps)(ListUser)
