import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk'
function reducer(state, action) {
    switch (action.type) {
        case "RESET_DATA":
            return {
                ...state,
                data: ''
            }

        case 'LOADING_DATA':
            return {
                ...state,
                data: 'LOADING...'
            }
        case 'LOADED_PAGE':
            return {
                ...state,
                data: action.payload
            }
        case 'LOADING_USER':
            return {
                ...state,
                data: 'LOADING...'
            }
        case 'LOADED_USER':
            return {
                ...state,
                data: action.payload
            }
        case 'SET_EMAIL':
            return {
                ...state,
                email: action.payload
            }
        case 'SET_PASSWORD':
            return {
                ...state,
                password: action.payload
            }
        case 'DISPLAY_CREATED_ID':
            return {
                ...state,
                postId: action.payload
            }
        case 'RESET_CREATE_USER':
            return {
                ...state,
                email: '',
                password: '',

            }
        default:
            return state

    }
}
// function 
export function resetData() {
    return {
        type: "RESET_DATA"
    }
}
function loadingPage() {
    return {
        type: 'LOADING_DATA'
    }
}
function loadedPage(data) {
    return {
        type: 'LOADED_PAGE',
        payload: data.data
    }
}
function loadingUser() {
    return {
        type: 'LOADING_USER'
    }
}
function loadedUser(data) {
    return {
        type: 'LOADED_USER',
        payload: data.data
    }
}
function displayCreatedId(id) {
    return {
        type: 'DISPLAY_CREATED_ID',
        payload: 'CREATED ID IS : ' + id
    }
}
function creatingUser() {
    return {
        type: 'DISPLAY_CREATED_ID',
        payload: 'CREATING...'
    }
}
export function setEmail(email) {
    return {
        type: 'SET_EMAIL',
        payload: email
    }
}
export function setPassword(password) {
    return {
        type: 'SET_PASSWORD',
        payload: password
    }
}
export function resetCreateUser() {
    return {
        type: 'RESET_CREATE_USER'
    }
}
export function fetchPage(url) {
    return function (dispatch) {
        dispatch(loadingPage())
        fetch(url).then((response) => {
            return response.json()
        }).then((data) => {
            dispatch(loadedPage(data))
        })
    }
}
export function fetchUser(url) {
    return function (dispatch) {
        dispatch(loadingUser())
        fetch(url).then((response) => {
            return response.json()
        }).then((data) => {
            dispatch(loadedUser(data))
        })
    }
}
export function post(email, password) {
    const data = {
        email: email,
        password: password
    }
    return function (dispatch) {
        dispatch(creatingUser())
        fetch('https://reqres.in/api/users', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }).then((res) => {
            return res.json()
        }).then((data) => {
            dispatch(displayCreatedId(data.id))
            console.log(data.id)
        }).catch((err) => {
            console.error(err)
        })
    }
}


const store = createStore(reducer, {
    data: '',
    email: '',
    password: '',
    postId: ''
}, applyMiddleware(thunk))

store.subscribe(() => {
    console.log(store.getState());
})
export default store